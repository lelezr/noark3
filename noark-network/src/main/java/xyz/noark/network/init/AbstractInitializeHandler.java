/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 * 
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.network.init;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import xyz.noark.core.annotation.Autowired;
import xyz.noark.core.annotation.Value;
import xyz.noark.core.network.NetworkListener;
import xyz.noark.core.network.Session;
import xyz.noark.core.network.SessionManager;
import xyz.noark.core.util.StringUtils;
import xyz.noark.network.InitializeHandler;
import xyz.noark.network.NetworkConstant;

/**
 * 抽象实现的初始化协议处理器.
 *
 * @since 3.1
 * @author 小流氓(176543888@qq.com)
 */
public abstract class AbstractInitializeHandler implements InitializeHandler {
	/** 监听器扩展方案 */
	@Autowired(required = false)
	private NetworkListener networkListener;
	/** 网络加密，默认不加密 */
	@Value(NetworkConstant.ENCRYPT)
	private boolean encrypt = false;
	/** 网络加密之密钥：默认配置为无边落木萧萧下，不尽长江滚滚来 */
	@Value(NetworkConstant.SECRET_KEY)
	private byte[] secretKey = StringUtils.utf8Bytes("do{ManyLeavesFly();YangtzeRiverFlows();}while(1==1);");

	@Override
	public void handle(ChannelHandlerContext ctx) {
		this.build(ctx.pipeline());

		// 只要第一个协议对了就要创建Session...
		Session session = SessionManager.createSession(ctx.channel().id(), key -> createSession(ctx, encrypt, secretKey));

		if (networkListener != null) {
			networkListener.channelActive(session);
		}
	}

	/**
	 * 创建Session.
	 * 
	 * @param ctx 链接上下文
	 * @param encrypt 是否加密
	 * @param secretKey 密钥
	 * @return Session对象
	 */
	protected abstract Session createSession(ChannelHandlerContext ctx, boolean encrypt, byte[] secretKey);

	/**
	 * 判定具体协议后再重装ChannelPipeline对象.
	 * 
	 * @param pipeline ChannelPipeline对象
	 */
	protected abstract void build(ChannelPipeline pipeline);
}